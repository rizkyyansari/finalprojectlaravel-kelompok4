<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Platform;
use App\Models\Genre;
use App\Models\Game;
use File;
use RealRashid\SweetAlert\Facades\Alert;

class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $game = Game::all();
        return view ('index', compact('game'));
    }

    // {
    //     $game = Game::get();
    //     return view ('pages.game.index', ['game' => $game]);
    // }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

       $genre = Genre::get();
       $platform = Platform::get();
       return view('pages.game.create-game', ['genre' => $genre, 'platform' => $platform]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'poster' => 'required|image|mimes:png,jpg,jpeg',
            'genre_id' => 'required',
            'platform_id' => 'required'
        ]);

        $fileName = time() . '.' . $request->poster->extension();

        $request->poster->move(public_path('user_img'), $fileName);


        $game = new Game;
        $game->nama = $request->nama;
        $game->genre_id = $request->genre_id;
        $game->platform_id = $request->platform_id;
        $game->poster = $fileName;

        $game->save();

        Alert::success('Berhasil', 'Berhasil Menambahkan');

        return redirect('/game');


    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $game = Game::find($id);
        return view('pages.game.detail-game', ['game' => $game]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $game = Game::find($id);
        $genre = Genre::get();
        $platform = Platform::get();

        return view('pages.game.update-game', ['game' => $game, 'genre' => $genre, 'platform' => $platform]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'nama' => 'required',
            'poster' => 'image|mimes:png,jpg,jpeg',
            'genre_id' => 'required',
            'platform_id' => 'required'
        ]);

        $game= Game::find($id);
        if($request->has('poster')){
            $path = 'user_img/';

            File::delete($path. $game->poster);

            $fileName = time() . '.' . $request->poster->extension();

            $request->poster->move(public_path('user_img'), $fileName);

            $game->poster = $fileName;

            $game->save();

        }

        $game->nama = $request['nama'];
        $game->genre_id = $request['genre_id'];
        $game->platform_id = $request['platform_id'];
        $game->save();
        return redirect('/game');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $game = Game::find($id);

        $path = 'user_img/';
        File::delete($path. $game->poster);

        $game->review()->delete();
        $game->delete();

        return redirect('/game');
    }
}