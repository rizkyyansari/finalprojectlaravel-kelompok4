<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Genre;
use RealRashid\SweetAlert\Facades\Alert;

class GenreController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $genre = Genre::all();
        return view('pages.genre.index', ['genre' => $genre]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $genre = Genre::all();
        return view('pages.genre.create', ['genre' => $genre]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama'=>'required|min:3|max:255|unique:platform,nama'
        ],[
            'nama.required'=>"Nama Platform harus diisi",
            'nama.min'=>"Nama Platform minimal 3 karakter",
            'nama.max'=>"Nama Platform maksimal 255 karakter",
            'nama.unique'=>"Nama Platform Sudah Ada"
        ]);
        //insert data ke table user
        $genre = new Genre;
        $genre->nama= $request->input('nama');
        $genre->save();

        Alert::success('Berhasil', 'Berhasil Menambahkan');

        return redirect('/genre');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $genre = Genre::find($id);
        return view('pages.genre.detail', ['genre' => $genre]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $genre = Genre::find($id);
        return view('pages.genre.edit', ['genre' => $genre]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request  $request, $id)
    {
        $request->validate([
            'nama'=>'required|min:3|max:255|unique:platform,nama'
        ],[
            'nama.required'=>"Nama Platform harus diisi",
            'nama.min'=>"Nama Platform minimal 3 karakter",
            'nama.max'=>"Nama Platform maksimal 255 karakter",
            'nama.unique'=>"Nama Platform Sudah Ada"
        ]);

        $genre = Genre::findOrFail($id);
        $genre->nama = $request->input('nama');

        $genre->save();


        return redirect('/genre');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $genre = Genre::find($id);

        $genre -> delete();

        return redirect('/genre');
    }
}
