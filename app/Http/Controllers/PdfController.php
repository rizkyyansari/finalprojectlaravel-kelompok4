<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;
use App\Models\User;

class PdfController extends Controller
{
    public function generatePdf()
    {
        $user = User::get();

        $data = [
            'title' => 'LIST DATA USER',
            'date' => date('m/d/Y'),
            'user' => $user
        ];

        $pdf = Pdf::loadView('pages.user.user-list-pdf', $data);
        return $pdf->download('data-user.pdf');
    }
}
