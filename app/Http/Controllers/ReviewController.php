<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Review;

class ReviewController extends Controller
{
    public function store ($game_id, Request $request) {
        $request->validate([
            'content' => 'required|min:4'
        ]);
        
        $id_user = Auth::id();

        $review = new Review;

        $review->content = $request['content'];
        $review->game_id = $game_id;
        $review->user_id = $id_user;

        $review->save();

        return redirect('/game/' . $game_id);
    }
}
