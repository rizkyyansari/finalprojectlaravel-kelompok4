<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use RealRashid\SweetAlert\Facades\Alert;

class AuthController extends Controller
{
    // public function indexLogin(){
    //     return view ('auth.index-login');
    // }


    public function Authenticate(Request $request){
        $credentials = $request->validate([
            'email' => 'required',
            'password' => 'required',
        ],[
            'email.required'=>'Email tidak boleh kosong!',
            'password.required'=>'Password tidak boleh kosong!'
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return redirect()->intended('/dashboard');
        }

        return redirect(route('login'))->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ])->withInput($request->only('email'));
    }

    public function Register (Request $request){
        $request->validate([
            'nama'=>'required|min:3|max:255',
            'email'=>'required|unique:users,email',
            'password'=>'required'
        ],[
            'nama.required'=>"Nama harus diisi",
            'nama.min'=>"Nama minimal 3 karakter",
            'nama.max'=>"Nama maksimal 255 karakter",

            'email.required'=> "Email wajib diisi",
            'email.unique'=> "Email sudah terdaftar",

            'password.required'=> "Password wajib diisi"
        ]);
        //insert data ke table user
        $users = new User;
        $users->nama= $request->input('nama');
        $users->email= $request->input('email');
        $users->password= bcrypt($request->input('password'));
        $users->save();
        
        Alert::success('Register Berhasil', 'Silahkan Login');

        return redirect('/');

    }

    public function logout(Request $request): RedirectResponse{
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }

}

