<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\profile;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class manageUsersController extends Controller
{
    public function indexUser(){
        $users = User::all();
        return view('pages.user.manage-user', ['users' => $users]);
    }

    public function createUser(){
        return view ('pages.user.create-user');
    }

    public function storeUser(Request $request){
        $request->validate([
            'nama'=>'required|min:3|max:255',
            'email'=>'required|unique:users,email',
            'password'=>'required'
        ],[
            'nama.required'=>"Nama harus diisi",
            'nama.min'=>"Nama minimal 3 karakter",
            'nama.max'=>"Nama maksimal 255 karakter",

            'email.required'=> "Email wajib diisi",
            'email.unique'=> "Email sudah terdaftar",

            'password.required'=> "Password wajib diisi"
        ]);
        //insert data ke table user
        $users = new User;
        $users->nama= $request->input('nama');
        $users->email= $request->input('email');
        $users->password= bcrypt($request->input('password'));
        $users->save();

        $profile = new profile;
        $profile->users_id = $users->id;
        $profile->save();

        Alert::success('Berhasil', 'Berhasil Menambahkan');

        return redirect('/index-user');
    }

    public function editUser($id){

    $users = User::findOrFail($id);
    $profile = $users->profile;
    return view('pages.user.edit-user', compact('users', 'profile'));
    }


    public function detailUser($id){
        $users = User::findOrFail($id);
        $profile = $users->profile;
        return view('pages.user.detail-user', compact('users', 'profile'));
    }

    public function updateUser(Request  $request, $id) {

        $request->validate([
            'nama' => 'required|string',
            'email' => 'required|email',
            'alamat' => 'required|string',
            'nohp' => 'required|string'
        ]);

        // Update user data
        $users = User::findOrFail($id);
        $users->nama = $request->input('nama');
        $users->email = $request->input('email');

        $users->save();

        $profile = Profile::updateOrCreate(
            ['users_id' => $id],
            ['alamat' => $request->input('alamat'), 'nohp' => $request->input('nohp')]
        );

        return redirect('/index-user');
    }

    public function deleteUser($id){
        $users = User::find($id);
        $users->profile()->delete();

        $users -> delete();

        return redirect('/index-user');
    }
}
