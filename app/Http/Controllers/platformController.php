<?php

namespace App\Http\Controllers;

use App\Models\Platform;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class platformController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function indexPlatform()
    {
        $platform = Platform::all();
        return view('pages.platform.index-platform', ['platform' => $platform]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function createPlatform()
    {
        return view ('pages.platform.create-platform');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function storePlatform(Request $request)
    {
        $request->validate([
            'nama'=>'required|min:3|max:255|unique:platform,nama'
        ],[
            'nama.required'=>"Nama Platform harus diisi",
            'nama.min'=>"Nama Platform minimal 3 karakter",
            'nama.max'=>"Nama Platform maksimal 255 karakter",
            'nama.unique'=>"Nama Platform Sudah Ada"
        ]);
        //insert data ke table user
        $platform = new Platform;
        $platform->nama= $request->input('nama');
        $platform->save();

        Alert::success('Berhasil', 'Berhasil Menambahkan');

        return redirect('/index-platform');
    }

    /**
     * Display the specified resource.
     */
    public function detailPlatform($id)
    {
        $platform = Platform::findOrFail($id);
        return view('pages.platform.detail-platform', compact('platform'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function editPlatform($id)
    {
        $platform = Platform::findOrFail($id);
        return view('pages.platform.edit-platform', compact('platform'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function updatePlatform(Request  $request, $id)
    {
        $request->validate([
            'nama'=>'required|min:3|max:255|unique:platform,nama'
        ],[
            'nama.required'=>"Nama Platform harus diisi",
            'nama.min'=>"Nama Platform minimal 3 karakter",
            'nama.max'=>"Nama Platform maksimal 255 karakter",
            'nama.unique'=>"Nama Platform Sudah Ada"
        ]);

        // Update user data
        $platform = Platform::findOrFail($id);
        $platform->nama = $request->input('nama');

        $platform->save();


        return redirect('/index-platform');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function deletePlatform($id)
    {
        $platform = Platform::find($id);

        $platform -> delete();

        return redirect('/index-platform');
    }
}
