<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    use HasFactory;
    protected $table = 'game';
    protected $fillabe = ['nama', 'poster', 'platform_id', 'genre_id'];

    public function review()
    {
        return $this->hasMany(Review::class);
    }
}
