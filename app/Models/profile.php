<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class profile extends Model
{
    protected $table = "profile";
    protected $fillable = ["alamat", "nohp","users_id"];

    use HasFactory;

    public function user()
{
    return $this->belongsTo(User::class, 'users_id');
}

}
