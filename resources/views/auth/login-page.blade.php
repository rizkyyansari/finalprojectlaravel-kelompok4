<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
  </head>
  <body>
    <link rel="stylesheet" href="{{asset('/template/css/login-page.css')}}">
    <div class="background-wrap">
      <div class="background"></div>
    </div>
    <form id="accesspanel" action="{{route('login')}}" method="post"> @csrf <h1 id="litheader">Please Login</h1>
      <div class="inset">
        <p>
          <Label>Email</Label>
          <input type="text" name="email" id="email" placeholder="Email address"> @error('email')
        <div class="alert alert-danger">
          {{ $message }}
        </div> @enderror </p>
        <p>
          <Label class="password-label">Password</Label>
          <input type="password" name="password" id="password" placeholder="type password">
        <div style="text-align: center;">
          {{-- <div class="checkboxouter">
												<input type="checkbox" name="rememberme" id="remember" value="Remember">
													<label class="checkbox"></label>
												</div> --}}
          {{-- <label for="remember">Remember me for 14 days</label> --}}
        </div>
        <input class="loginLoginValue" type="hidden" name="service" value="login" />
      </div>
      <p class="p-container">
        <input type="submit" name="Login" id="go" value="Login">
      </p>
    </form>
  </body>
</html>
