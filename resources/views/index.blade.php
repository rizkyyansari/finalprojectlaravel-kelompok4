<!DOCTYPE html>
<html lang="zxx">
<head>
	<title>Kelompok 4</title>
	<meta charset="UTF-8">
	<meta name="description" content="Game Warrior Template">
	<meta name="keywords" content="warrior, game, creative, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicon -->
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.0/css/line.css" />


	<!-- Stylesheets -->
	<link rel="stylesheet" href="{{asset('template/css/bootstrap.min.css')}}"/>
	<link rel="stylesheet" href="{{asset('template/css/font-awesome.min.css')}}"/>
	<link rel="stylesheet" href="{{asset('template/css/owl.carousel.css')}}"/>
	<link rel="stylesheet" href="{{asset('template/css/style.css')}}"/>
	<link rel="stylesheet" href="{{asset('template/css/animate.css')}}"/>
    <link rel="stylesheet" href="{{asset('template/css/login.css')}}"/>
    @stack('styles')

	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>
	<!-- Page Preloder -->
	{{-- <div id="preloder">
		<div class="loader"></div>
	</div> --}}

	<!-- Header section -->
	<header class="header-section">
		<div class="container">
			<!-- logo -->
			<a class="site-logo" href="index.html">
				<img src="{{asset('template/img/logo_rentgame.png')}}" style="width: 100px; height: auto;" alt="">
			</a>
			<div class="user-panel">
				{{-- <a id="form-open">Login</a>  --}}
                @guest
                <a href="/login-page">Login</a> /  <a href="/register-page">Register</a>
                @endguest
                @auth

                <a href="/dashboard">Dashboard :</a>
                <a href="#">{{Auth::user()->email}}</a>

                @endauth

			</div>

            {{-- /  <a href="#">Register</a> --}}
			<!-- responsive -->
			<div class="nav-switch">
				<i class="fa fa-bars"></i>
			</div>

			<!-- site menu -->
			<nav class="main-menu">
				<ul>
					<li><a href="/">Home</a></li>
					<li><a href="/">Games</a></li>
					<li><a href="/index-platform">Platforms</a></li>
					<li><a href="/genre">Genres</a></li>
				</ul>
			</nav>
		</div>
	</header>
	<!-- Header section end -->
    {{-- @include('auth.login') --}}

	<!-- Hero section -->
	<section class="hero-section">
		<div class="hero-slider owl-carousel">
			<div class="hs-item set-bg" data-setbg="{{asset('template/img/slider-1.jpg')}}">
				<div class="hs-text">
					<div class="container">
						<img src="{{asset('template/img/logo_rentgame.png')}}" style="width: 400px; height: auto;" alt="logo rent game">
						<br>
						<p>By Gamers for gamers, Platform peminjangan dan sewa game  <br> terbaru di Indonesia</p>
						<a href="#" class="site-btn">Read More</a>
					</div>
				</div>
			</div>
			<div class="hs-item set-bg" data-setbg="{{asset('template/img/slider-2.jpg')}}">
				<div class="hs-text">
					<div class="container">
						<h2>By : <span>Kelompok 4</span></h2>
						<ol style="margin-bottom: 25px; color: white;">
							<li>Muhammad Rizky Ansari (https://t.me/rizkyyansari)</li>
							<li>Robertho Wicaksono (https://t.me/obitwicaksono)</li>
							<li>Muhammad Rizqi Fakhri Kurniawan (rizqifakhri1)</li>
						</ol>
						<a href="/game" class="site-btn">Read More</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Hero section end -->


	<!-- Latest news section -->
	{{-- @include('partial.running-text') --}}
	<!-- Latest news section end -->


	<!-- Feature section -->
	{{-- @include('partial.featured-section') --}}
	<!-- Feature section end -->


	<!-- Recent game section  -->
	@include('partial.gamesection')
	<!-- Recent game section end -->


	<!-- Tournaments section -->
	{{-- @include('partial.tournament') --}}
	<!-- Tournaments section bg -->


	<!-- Review section -->
	{{-- @include('partial.recent-review') --}}
	<!-- Review section end -->


	<!-- Footer top section -->
	{{-- @include('partial.footer-top') --}}
	<!-- Footer top section end -->


	<!-- Footer section -->
	@include('partial.footer')
	<!-- Footer section end -->


	<!--====== Javascripts & Jquery ======-->
	<script src="{{asset('template/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{asset('template/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('template/js/owl.carousel.min.js')}}"></script>
	<script src="{{asset('template/js/jquery.marquee.min.js')}}"></script>
	<script src="{{asset('template/js/main.js')}}"></script>
    <script src="{{asset('template/js/script.js')}}"></script>
    @stack('scripts')
	@include('sweetalert::alert')
    </body>
</html>
