@extends('dashboard.dashboard')

@section('judulmenu')
Manage Data User
@endsection
@push('scripts')
  <script src="https://code.jquery.com/jquery-3.7.1.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/js/bootstrap.bundle.min.js"></script>
  <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
  <script src="https://cdn.datatables.net/2.0.2/js/dataTables.bootstrap5.js"></script>
  <script>
    $(document).ready(function () {
      $('#example').DataTable();
    });
  </script>
@endpush
@section('content')
<table id="example" class="table table-striped" style="width:100%">
    <thead>
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Position</th>
            <th>Office</th>
            <th>Age</th>
            <th>Start date</th>
            <th>Salary</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>Rizky</td>
            <td>Edinburgh</td>
            <td>61</td>
            <td>61</td>
            <td>61</td>
            <td>2011-04-25</td>
            <td>
              <form action="" method="POST">
                @csrf
                @method('DELETE')
                  <a href="" class="btn btn-info btn-sm">Detail</a>
                  <a href="" class="btn btn-primary btn-sm">Edit</a>
                  <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                  {{-- <a href="" class="btn btn-danger"><i class="fas fa-trash-alt"></i>hapus</a> --}}
                </form>
            </td>
        </tr>
    </tbody>
  </table>

@endsection
