@extends('layouts.dashboard')

@section('nama-table')
@endsection

@section('content-table')
    <div>
        <h2>Halaman Tambah Game</h2>
        <form action="/game" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="form-group">
                <label>Nama</label>
                <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama Game">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label>Poster</label>
                <input type="file" class="form-control" name="poster" placeholder="Masukkan file poster">
                @error('poster')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label>Genre</label>
                <select name="genre_id" class="form-control" id="">
                    <option value="">--Pilih Genre--</option>
                    @forelse ($genre as $item)
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                    @empty
                        <option value="">Tidak ada Data Genre</option>
                    @endforelse
                </select>
                @error('genre_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label>Platform</label>
                <select name="platform_id" class="form-control" id="">
                    <option value="">--Pilih Platform--</option>
                    @forelse ($platform as $item)
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                    @empty
                        <option value="">Tidak ada Data Platform</option>
                    @endforelse
                </select>
                @error('platform_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary" style="margin-top:0.5cm">Tambah</button>
        </form>
    @endsection
