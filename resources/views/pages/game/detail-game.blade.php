{{-- @extends('layouts.dashboard')

@section('nama-table')
@endsection

@section('content-table')


    <div class="card">
        <img class="card-img-top" src="{{asset('user_img/' . $game->poster)}}" alt="Card image cap" style="height: auto; width: 300px;">
        <div class="card-body">
        <h5 class="card-title">Judul Game : {{ \App\Models\Platform::find($game->platform_id)->nama }}</h5>
        <p class="card-text">Genre Game : {{ \App\Models\Genre::find($game->genre_id)->nama }}.</p>
        <p>Platform : {{ \App\Models\Platform::find($game->platform_id)->nama }}</p>
        <a href="/" class="btn btn-primary">Back</a>
        </div>
    </div>

@endsection --}}

@extends('layouts.master')
@section('title', 'Detail Game')
@section('content')
    <header class="header-section">
        <div class="container">
            <!-- logo -->
            <a class="site-logo" href="index.html">
                <img src="{{ asset('template/img/logo_rentgame.png') }}" style="width: 100px; height: auto;" alt="">
            </a>
            <div class="user-panel">
                {{-- <a id="form-open">Login</a>  --}}
                @guest
                    <a href="/login-page">Login</a> / <a href="/register-page">Register</a>
                @endguest
                @auth

                    <a href="/dashboard">Dashboard :</a>
                    <a href="#">{{ Auth::user()->email }}</a>

                @endauth

            </div>

            {{-- /  <a href="#">Register</a> --}}
            <!-- responsive -->
            <div class="nav-switch">
                <i class="fa fa-bars"></i>
            </div>

            <!-- site menu -->
            <nav class="main-menu">
                <ul>
                    <li><a href="/">Home</a></li>
                    <li><a href="/">Games</a></li>
                    <li><a href="#">Keranjang</a></li>
                    <li><a href="#">Genres</a></li>
                </ul>
            </nav>
        </div>
    </header>

    <section>
        <div class="d-flex justify-content-center mt-3">
            <div class="card p-2 d-flex justify-content-center" style="width: 18rem;">
                <img src="{{ asset('user_img/' . $game->poster) }}" alt="Card image cap" class="card-img-top"
                    style="height: 400px; object-fit: cover" alt="...">
            </div>
        </div>

        <div class="d-flex justify-content-center mt-5">
            <div class="text-white text-center">
                <h5>Judul Game : {{ \App\Models\Platform::find($game->platform_id)->nama }}</h5>
                <p>Genre Game : {{ \App\Models\Genre::find($game->genre_id)->nama }}.</p>
                <p>Platform : {{ \App\Models\Platform::find($game->platform_id)->nama }}</p>
            </div>
        </div>

        <div class="row" style="margin-bottom: 30px">
            <div class="col-3"></div>
            <div class="col-6">
                @forelse ($game->review as $item)
                    <div class="card mt-3">
                        <div class="card-header bg-dark text-white">
                            {{ $item->user->nama }}
                        </div>
                        <div class="card-body">
                            <p class="card-text">{{ $item->content }}</p>
                        </div>
                    </div>
                @empty
                    <h4 class="text-white">Tidak ada komentar</h4>
                @endforelse
            </div>
            <div class="col-3"></div>
        </div>
        
        <div class="row">
            <div class="col-3"></div>
            <div class="col-6">
                @auth
                    <form action="/review/{{ $game->id }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <textarea class="form-control @error('content') is-invalid @enderror" name="content" cols="30" rows="10"
                                placeholder="Buat Komentar..."></textarea>
                            @error('content')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    @endauth
            </div>
            <div class="col-3"></div>
        </div>
    </section>
@endsection
