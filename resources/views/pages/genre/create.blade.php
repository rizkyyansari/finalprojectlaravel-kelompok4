@extends('layouts.dashboard')

@section('nama-table')
@endsection

@section('content-table')
    <div>
        <h2>Tambah Genre</h2>
        <form action="/genre" method="POST">
            @csrf

            <div class="form-group">
                <label>Nama Genre</label>
                <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama Genre">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary" style="margin-top:0.5cm">Tambah</button>
        </form>
    @endsection
