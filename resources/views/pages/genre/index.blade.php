@extends('layouts.dashboard')


@section('nama-table')
    Manage Genre
@endsection

@push('scripts')
  <script src="https://code.jquery.com/jquery-3.7.1.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/js/bootstrap.bundle.min.js"></script>
  <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
  <script src="https://cdn.datatables.net/2.0.2/js/dataTables.bootstrap5.js"></script>
  <script>
    $(document).ready(function () {
      $('#myTable').DataTable();
    });
  </script>
@endpush

@push('styles')
<link href="https://cdn.datatables.net/v/bs4/dt-2.0.1/datatables.min.css" rel="stylesheet">
@endpush


@section('content-table')

<div>
    <a href="/genre/create" class="btn btn-primary btn-sm" style="float: right; margin-top: 0.2cm; margin-left: 0.2cm"><i class="fas fa-plus"></i>Tambah Genre</a>
</div>
<table id="myTable" class="table table-striped table-sm">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama Genre</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($genre as $key  => $value)
        <tr>
            <td>{{$loop->iteration}}.</td>
            <td>{{$value->nama}}</td>
            <td>
              <form action="/genre/{{$value->id}}" method="POST">
                @csrf
                @method('DELETE')
                  <a href="/genre/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
                  <a href="/genre/{{$value->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                    {{-- <a href="" class="btn btn-danger"><i class="fas fa-trash-alt"></i>hapus</a> --}}
                  </form>
              </td>
          </tr>
        @empty

        @endforelse

    </tbody>
  </table>
@endsection
