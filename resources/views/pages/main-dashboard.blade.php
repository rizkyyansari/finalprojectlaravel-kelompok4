@extends('layouts.dashboard')

@section('content-table')
    <div class="d-flex justify-content-around mt-3">
        <div class="card" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">Manage Users</h5>
                <p class="card-text">User data can be managed here. Creation, updating, and deletion of user data are all
                    possible.</p>
                <a href="/index-user" class="card-link">Manage Users</a>
            </div>
        </div>
        <div class="card" style="width: 18.4rem;">
            <div class="card-body">
                <h5 class="card-title">Manage Game Platforms</h5>
                <p class="card-text">This section allows you to manage the list of game platforms. The game platform can be
                    created, updated, and deleted.</p>
                <a href="/index-platform" class="card-link">Manage Platforms</a>
            </div>
        </div>
        <div class="card" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">Manage Game Genres</h5>
                <p class="card-text">This section allows you to manage your game genres. You can create, update, and delete
                    game genres.</p>
                <a href="/genre" class="card-link">Manage Genres</a>
            </div>
        </div>
    </div>
@endsection
