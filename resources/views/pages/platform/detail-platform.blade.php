@extends('layouts.dashboard')

@section('nama-table')

@endsection

@section('content-table')
<div>
    <h2>Detail Platform</h2>
    <div class="form-group">
        <label>Nama Platform</label>
        <input type="text" class="form-control"  value="{{$platform->nama}}" disabled>
    </div>
    <a href="/index-user" class="btn btn-secondary" style="margin-top:0.5cm">Kembali</a>
@endsection
