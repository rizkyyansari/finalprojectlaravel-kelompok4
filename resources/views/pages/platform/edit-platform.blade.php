@extends('layouts.dashboard')

@section('nama-table')

@endsection

@section('content-table')
<div>
    <h2>Edit Data Platform</h2>
        <form action="/update-platform/{{$platform->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label>Nama Platform</label>
                <input type="text" class="form-control" name="nama"  placeholder="Masukkan Nama Platform" value="{{$platform->nama}}">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary" style="margin-top:0.5cm">Edit</button>
        </form>
</div>
@endsection
