@extends('layouts.dashboard')

@section('nama-table')

@endsection

@section('content-table')
<div>
    <h2>Tambah Data User</h2>
        <form action="/store-user" method="POST">
            @csrf
            <div class="form-group">
                <label>Nama User</label>
                <input type="text" class="form-control" name="nama"  placeholder="Masukkan Nama User">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="email" class="form-control" name="email" placeholder="Masukkan Email">
                @error('email')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" name="password" placeholder="Masukkan Password">
                @error('password')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary" style="margin-top:0.5cm">Tambah</button>
        </form>
@endsection
