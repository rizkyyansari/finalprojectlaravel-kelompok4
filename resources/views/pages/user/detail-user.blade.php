@extends('layouts.dashboard')

@section('nama-table')

@endsection

@section('content-table')
<div>
    <h2>Detail User</h2>
    <div class="form-group">
        <label>Nama User</label>
        <input type="text" class="form-control"  value="{{$users->nama}}" disabled>
    </div>
    <div class="form-group">
        <label>Email</label>
        <input type="text" class="form-control"  value="{{$users->email}} Tahun" disabled>
    </div>
    <div class="form-group">
        <label>Alamat</label>
        <input type="text" class="form-control"  value="{{$profile->alamat}}" disabled>
    </div>
    <div class="form-group">
        <label>No HP</label>
        <input type="text" class="form-control"  value="{{$profile->nohp}}" disabled>
    </div>
    <a href="/index-user" class="btn btn-secondary" style="margin-top:0.5cm">Kembali</a>
@endsection
