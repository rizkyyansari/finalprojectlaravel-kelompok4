@extends('layouts.dashboard')

@section('nama-table')

@endsection

@section('content-table')
<div>
    <h2>Edit Data User</h2>
        <form action="/update-user/{{$users->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label>Nama User</label>
                <input type="text" class="form-control" name="nama"  placeholder="Masukkan Nama User" value="{{$users->nama}}">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="email" class="form-control" name="email" placeholder="Masukkan Email" value="{{$users->email}}">
                @error('email')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Alamat</label>
                <input type="text" class="form-control" name="alamat" placeholder="" value="{{ optional($profile)->alamat }}">
                @error('alamat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>No HP</label>
                <input type="text" class="form-control" name="nohp" placeholder="" value="{{optional($profile)->nohp}}">
                @error('nohp')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary" style="margin-top:0.5cm">Edit</button>
        </form>
</div>
@endsection
