@extends('layouts.dashboard')


@section('nama-table')
    Manage User
@endsection

@push('scripts')
    <script src="https://code.jquery.com/jquery-3.7.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.bootstrap5.js"></script>
    <script>
        $(document).ready(function() {
            $('#myTable').DataTable();
        });
    </script>
@endpush


@push('styles')
    <link href="https://cdn.datatables.net/v/bs4/dt-2.0.1/datatables.min.css" rel="stylesheet">
@endpush


@section('content-table')
    <div>
        <a href="/create-user" class="btn btn-primary btn-sm" style="float: right; margin-top: 0.2cm; margin-left: 0.2cm"><i
                class="fas fa-plus"></i>Tambah User</a>
    </div>
    <table id="myTable" class="table table-striped table-sm">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Nama</th>
                <th scope="col">Email</th>
                <th scope="col">No HP</th>
                <th scope="col">Alamat</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($users as $key  => $value)
                <tr>
                    <td>{{ $loop->iteration }}.</td>
                    <td>{{ $value->nama }}</td>
                    <td>{{ $value->email }}</td>
                    <td>{{ optional($value->profile)->alamat }}</td>
                    <td>{{ optional($value->profile)->nohp }}</td>
                    <td>
                        <form action="/delete-user/{{ $value->id }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="/detail-user/{{ $value->id }}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/edit-user/{{ $value->id }}" class="btn btn-primary btn-sm">Edit</a>
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                            {{-- <a href="" class="btn btn-danger"><i class="fas fa-trash-alt"></i>hapus</a> --}}
                        </form>
                    </td>
                </tr>
            @empty
            @endforelse

        </tbody>
    </table>

    <a href="/generate-pdf" class="btn btn-primary btn-sm">Download PDF</a>
@endsection
