<section class="recent-game-section spad set-bg" data-setbg="{{asset('template/img/recent-game-bg.png')}}">
    <div class="container">
        <div class="section-title">
            <h2>List RentGame</h2>
            @auth
            <a href="/game/create" class="site-btn" style="margin-top: 25px;">Tambahkan Game</a>
            @endauth

        </div>
        <div class="row">
            @forelse ($game as $item)
            <div class="col-lg-4 col-md-6">
                <div class="recent-game-item" style="margin-top: 30px;">
                    <div class="rgi-thumb set-bg" style="height: 350px" data-setbg="{{asset('user_img/' . $item->poster)}}">
                        <div class="cata new">{{ \App\Models\Platform::find($item->platform_id)->nama }}</div>
                    </div>
                    <div class="rgi-content">
                        <h5>{{$item->nama}}</h5>
                        <p>{{ \App\Models\Genre::find($item->genre_id)->nama }}</p>
                        @auth
                        <a href="/game/{{$item->id}}" class="site-btn btn-sm">Detail</a>
                        <a href="/game/{{$item->id}}/edit" class="site-btn btn-sm my-2" style="background-color: rgb(108, 28, 220)">Edit</a>
                        <form action="/game/{{$item->id}}/delete" method="POST">
                            @csrf
                            @method('delete')
                            <input type="submit" class="site-btn btn-sm" style="background-color: rgb(225, 14, 95)" value="Delete">
                        </form>
                        @endauth
                        @guest
                        <a href="/game/{{$item->id}}" class="site-btn btn-sm">Detail</a>
                        @endguest
                    </div>
                </div>
            </div>
            @empty
            @auth
            <p style="text-align: center; display: block; margin: auto;">Game sedang kosong, Silakan Klik Tambahkan Game</p>
            @endauth

            @guest
            <p style="text-align: center; display: block; margin: auto;">Game sedang kosong</p>
            @endguest


            @endforelse

        </div>
    </div>
</section>
