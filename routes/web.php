<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\Authenticate;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\manageUsersController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\platformController;
use App\Http\Controllers\GameController;
use Illuminate\Auth\AuthenticationException;
use App\Http\Controllers\ReviewController;



Route::get('/', function () {
    return view('index');
});

Route::get('/login-page', function () {
    return view('auth.login-page');
});
Route::get('/register-page', function () {
    return view('auth.register-page');
});


//auth

Route::post('/login-page', [AuthController::class, 'Authenticate'])->name('login');
Route::post('/register', [AuthController::class, 'Register'])->name('register');
Route::post('/logout',[AuthController::class, 'logout']);


Route::middleware(['auth'])->group(function () {

    Route::get('/dashboard', function () {
        return view('pages.main-dashboard');
    });

    //CRUD users
    Route::get('/index-user',[manageUsersController::class, 'indexUser']);
    Route::get('/create-user',[manageUsersController::class, 'createUser']);
    Route::post('/store-user',[manageUsersController::class, 'storeUser']);
    Route::get('/edit-user/{users_id}',[manageUsersController::class, 'editUser']);
    Route::get('/detail-user/{users_id}',[manageUsersController::class, 'detailUser']);
    Route::put('/update-user/{users_id}',[manageUsersController::class, 'updateUser']);
    Route::delete('/delete-user/{users_id}',[manageUsersController::class, 'deleteUser']);


    //CRUD Platform
    Route::get('/index-platform',[platformController::class, 'indexPlatform']);
    Route::get('/create-platform',[platformController::class, 'createPlatform']);
    Route::post('/store-platform',[platformController::class, 'storePlatform']);
    Route::get('/edit-platform/{platform_id}',[platformController::class, 'editPlatform']);
    Route::get('/detail-platform/{platform_id}',[platformController::class, 'detailPlatform']);
    Route::put('/update-platform/{platform_id}',[platformController::class, 'updatePlatform']);
    Route::delete('/delete-platform/{platform_id}',[platformController::class, 'deletePlatform']);

    //CRUD Genre
    //Create
    Route::get('/genre/create',[GenreController::class, 'create']);
    Route::post('/genre',[GenreController::class, 'store']);
    //Read
    Route::get('/genre',[GenreController::class, 'index']);
    Route::get('/genre/{id}',[GenreController::class, 'show']);
    //Update
    Route::get('/genre/{id}/edit',[GenreController::class, 'edit']);
    Route::put('genre/{id}',[GenreController::class, 'update']);
    //Delete
    Route::delete('/genre/{id}',[GenreController::class, 'destroy']);



    //CRUD Game
    Route::resource('game', GameController::class);

    Route::get('/game/create',[GameController::class, 'create']);
    Route::post('/game',[GameController::class, 'store']);

    Route::get('/game/{id}/edit',[GameController::class, 'edit']);
    Route::put('game/{id}',[GameController::class, 'update']);
    Route::get('/game/{id}',[GameController::class, 'show']);

    Route::delete('/game/{id}/delete',[GameController::class, 'destroy']);

    //Library DOMPDF
    Route::get('/generate-pdf', [App\Http\Controllers\PdfController::class, 'generatePdf']);

    // Review Game
    Route::post('/review/{game_id}', [ReviewController::class, 'store']);
});

Route::get('/',[GameController::class, 'index']);

